#!/usr/bin/python
# -*- coding: ascii -*-
import socket
import struct
import json
import sys
import urllib2
import os
from modules.SFS import generate_key, XOR, SfsException, file_to_string, output_matrix
from modules.CryptoUtils import CryptoUtils, CryptoException, generate_RSA_keys
import femida_checker
from femida_checker import Result



def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('ascii')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv
def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('ascii')
        if isinstance(value, unicode):
            value = value.encode('ascii')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv
def ascii_encode_params(strings):
    for i in xrange(len(strings)):
        strings[i] = strings[i].encode('ascii')
    return strings


class Connector:
    def __init__(self, host, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        self.s = s
    def read_message(self):
        received_buffer = self.s.recv(4)
        if len(received_buffer) < 4:
            raise Exception('Error while receiving data')
        to_receive = struct.unpack('>I', received_buffer[0:4])[0]
        received_buffer = ''
        while (len(received_buffer) < to_receive):
            received_buffer += self.s.recv(to_receive - len(received_buffer))
        return received_buffer

    def send_message(self, message):
        send_buffer = struct.pack('>I', len(message)) + message
        self.s.sendall(send_buffer)

    def close(self):
        self.s.close()

def parse_message(message):
    args = message.split(' ')
    cmd = args[0].strip()
    del args[0]
    return (cmd, args)

def read_settings(settings_file):
    settings = json.loads(open(settings_file, 'r').read(), object_hook=_decode_dict)
    if not 'checkerPrivateFileName' in settings:
        settings['checkerPrivateFileName'] = "checker.private"
    path = 'None'
    try:
        path = 'http://%s/%s.public' % (settings['pathToPublicKeys'], settings['checkerName'])
        response = urllib2.urlopen(path, timeout=3)
        key_str = response.read()
        if RSA.importKey(key_str):
            exists = True
    except Exception as ex:
        exists = False
    if not os.path.isfile(settings['checkerPrivateFileName']) or not exists:
        (public_key, private_key) = generate_RSA_keys()
        with open(settings['checkerPrivateFileName'], 'w+') as f:
            f.write(private_key)
        with open(os.path.join(settings['pathToPublicKeysFolder'], settings['checkerName'] + '.public'), 'w+') as f:
                f.write(public_key)

    return settings


#
# functions to put and get flag with/by flag_id
#
def put(flag, flag_id, team_name, host, port, logger, CU, settings):
    connector = None
    try:
        try:
            connector = Connector(host, port)
        except Exception as ex:
            logger.error('Failed to connect to %s:%d', host, port)
            logger.debug(str(ex), exc_info=True)
            return Result.DOWN

        try:
            SessionKey = CU.log_in(connector, team_name)
            connector.send_message(CU.encrypt('PUT ' + str(flag_id) + ' ' + flag, SessionKey))
            if CU.decrypt(connector.read_message(), SessionKey) != 'DONE':
                raise
        except Exception as ex:
            logger.error('Failed to commit the protocol')
            logger.debug(str(ex), exc_info=True)
            return Result.MUMBLE

        try:
            if flag != get2(flag_id, host, settings):
                logger.error('Failed to verify that the flag has been embedded in SFS')
                return Result.CORRUPT
            return Result.OK
        except Exception as ex:
            logger.error('Failed to retrieve the flag directly from the SFS')
            logger.debug(str(ex), exc_info=True)
            return Result.MUMBLE
        return Result.OK
    finally:
        if connector:
            connector.close()

def get(flag, flag_id, team_name, host, port, logger, CU, settings):
    # retrieve flag using the proto
    connector = None
    try:
        try:
            connector = Connector(host, port)
        except Exception as ex:
            logger.error('Failed to connect to %s:%d', host, port)
            logger.debug(str(ex), exc_info=True)
            return Result.DOWN

        try:
            SessionKey = CU.log_in(connector, team_name)
            connector.send_message(CU.encrypt('GET ' + str(flag_id), SessionKey))
            flag_proto = CU.decrypt(connector.read_message(), SessionKey)
        except Exception as ex:
            logger.error('Failed to commit the protocol')
            logger.debug(str(ex), exc_info=True)
            return Result.MUMBLE
    finally:
        if connector:
            connector.close()

    # check to see if it's in SFS
    try:
        flag_sfs = get2(flag_id, host, settings)
    except Exception as ex:
        logger.error('Failed to retrieve the flag directly from the SFS')
        logger.debug(str(ex), exc_info=True)
        return Result.MUMBLE

    # compare retrieved flags
    if flag == flag_proto and flag == flag_sfs:
        return Result.OK
    else:
        if flag != flag_proto:
            logger.error('The flag retreived using the protocol doesn\'t match the expected one')
        if flag != flag_sfs:
            logger.error('The flag retreived using the SFS doesn\'t match the expected one')
        return Result.CORRUPT

def get2(flag_id, host, settings):
    SfsPass = settings['teams'][host]['keyGenerationPassword']
    if flag_id >= settings['maxFiles']:
        raise SfsException('The asked file number %d exceeds the maximum number of files' % flag_id)

    response = urllib2.urlopen('http://%s:%d/shadow_files' % (host, settings['SfsFilesHTTPPort']), timeout=3)
    str = response.read()
    fileLength = settings['fileLength']
    C = [map(ord, str[i:i+fileLength])  for i in range(0, len(str), fileLength)]

    K = generate_key(settings['maxFiles'], settings['numOfCVs'], SfsPass)
    F = [0 for i in xrange(fileLength)]
    for i in xrange(len(K[flag_id])):
        if K[flag_id][i] != 0:
            F = XOR(F, C[i])
    return file_to_string(F)




class SampleChecker(femida_checker.Checker):

    def _push(self, team_ip, flag_id, flag):
        # read checker settings
        settings = read_settings('settings_checker.json')
        (team_ip, flag_id, flag) = ascii_encode_params([team_ip, flag_id, flag])


        flag_id = self.get_the_next_flag_id(team_ip, settings)

        try:
            CU = CryptoUtils('checker', settings['pathToPublicKeys'], settings['checkerPrivateFileName'], settings['p_str'], settings['g_str'])
        except (CryptoException) as ex:
            self.logger.error('Failed to do the job')
            self.logger.debug(str(ex), exc_info=True)
            return Result.CHECKER_ERROR
        teamName = settings['teams'][team_ip]['teamName']
        ret = put(flag, flag_id, teamName, team_ip, settings['servicePort'], self.logger, CU, settings)
        return (ret, flag_id)

    def get_the_next_flag_id(self, team_ip, settings):
        if not hasattr(self, 'flag_ids'):
            if os.path.isfile(settings['flagIDsFileName']):
                with open(settings['flagIDsFileName'], 'r') as f:
                    self.flag_ids = json.loads(f.read(), object_hook=_decode_dict)
            else:
                self.flag_ids = {}
        if team_ip not in self.flag_ids:
            self.flag_ids[team_ip] = 0
        ret = self.flag_ids[team_ip]
        self.flag_ids[team_ip] += 1
        self.flag_ids[team_ip] %= settings['fileLength']
        with open(settings['flagIDsFileName'], 'w+') as f:
            f.write(json.dumps(self.flag_ids, indent=4))
        return ret


    def _pull(self, team_ip, flag_id, flag):
        (team_ip, flag) = ascii_encode_params([team_ip, flag])
        if not type(flag_id) is int:
            flag_id = int(flag_id)

        # read checker settings
        settings = read_settings('settings_checker.json')
        try:
            CU = CryptoUtils('checker', settings['pathToPublicKeys'], settings['checkerPrivateFileName'], settings['p_str'], settings['g_str'])
        except (CryptoException) as ex:
            logger.error('Failed to do the job')
            logger.debug(str(ex), exc_info=True)
            return Result.CHECKER_ERROR
        teamName = settings['teams'][team_ip]['teamName']
        ret = get(flag, flag_id, teamName, team_ip, settings['servicePort'], self.logger, CU, settings)
        return ret




my_checker = SampleChecker()
my_checker.run()