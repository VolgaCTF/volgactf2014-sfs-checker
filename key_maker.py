#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import struct
import json
import os.path
import random
from datetime import datetime
import socket
import SocketServer



logger = None

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass
class ServiceServerHandler(SocketServer.BaseRequestHandler):
	def __init__(self, request, client_address, server):
		SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)


	def handle(self):
		try:
			try:
				with open('settings_checker.json', 'r') as f:
					settings = json.loads(f.read())
			except Exception as ex:
				print 'Failed to open settings_checker.json\n', ex
				logger.error(str(ex), exc_info=True)
				exit(1)


			logger.info('Accepted connection from %s' % self.client_address[0])

			# get team name and ip
			team_ip = self.client_address[0]
			team_name = 'team' + str(random.randint(5, 2000))

			additional_settings = json.loads(read_message(self.request))
			if not 'teams' in settings:
				settings['teams'] = {}
			settings['teams'][team_ip] = {
				'teamName': team_name,
				'keyGenerationPassword': additional_settings['keyGenerationPassword']
			}
			with open(os.path.join(settings['pathToPublicKeysFolder'], team_name + '.public'), 'w+') as f:
				f.write(additional_settings['publicKey'])
			send_message(self.request, team_name)
			with open('settings_checker.json', 'w+') as f:
				f.write(json.dumps(settings, indent=4))

			logger.info('Team name: %s, Team SFS pass: %s', team_name, additional_settings['keyGenerationPassword'])
		except Exception as ex:
			logger.error('Error: %s', ex, exc_info=True)
		return


#
# helper functions
#
def read_message(s):
	received_buffer = s.recv(4)
	if len(received_buffer) < 4:
		raise Exception('Error while receiving data')
	to_receive = struct.unpack('>I', received_buffer[0:4])[0]
	received_buffer = received_buffer[5:]
	while (len(received_buffer) < to_receive):
		received_buffer += s.recv(to_receive - len(received_buffer))
	return received_buffer

def send_message(s, message):
	send_buffer = struct.pack('>I', len(message)) + message
	s.sendall(send_buffer)








if __name__ == '__main__':
	try:
		logger = logging.getLogger(__name__)
		logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
	except Exception as ex:
		print 'Failed to open a log file\n', ex
	address = ('0.0.0.0', 9999)
	server = ThreadedTCPServer(address, ServiceServerHandler)
	server.serve_forever()