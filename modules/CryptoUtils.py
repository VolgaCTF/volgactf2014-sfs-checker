#!/usr/bin/python
# -*- coding: utf-8 -*-

from Crypto import Random
from Crypto.PublicKey import RSA

import Crypto.Cipher.AES as AES
import Crypto.Hash.HMAC as HMAC
import Crypto.Hash.SHA384 as SHA384

import gmpy2
from gmpy2 import mpz
import binascii
import random
import struct
import os
import urllib2




class CryptoException(Exception):
	pass
class CryptoUtils:
	# constants
	KeyLengthAES = 32
	RSA_KEY_LENGTH = 1024
	RSA_CHUNK_SIZE = 100

	def __init__(self, user_name, public_keys_path, private_key_file, p_str, g_str):
		self.name = user_name
		
		self.public_keys_path = public_keys_path
		self.PrivateKey = self.read_private_key(private_key_file)
		self.p = mpz(p_str)
		self.g = mpz(g_str)


	def authorise(self, s):
		# first stage
		# process message
		recv_message  = s.read_message()										#  [{Nc, g_x}^kT, C]^kC
		(signature, recv_message) = unpack_number(recv_message)
		(total_message, recv_message) = unpack_bytes(recv_message)
		(encrypted_message, recv_message) = unpack_bytes(total_message)
		(client_name, recv_message) = unpack_bytes(recv_message)
		ClientKey = self.read_public_key(client_name)
		if not RSA_verify(total_message, signature, ClientKey):
			raise CryptoException('Signature verification failed')
		message = RSA_decrypt(encrypted_message, self.PrivateKey)
		(Nc, message) = unpack_number(message)
		(g_x, message) = unpack_number(message)
		
		
		# second stage
		# compose the response
		Nt = generate_nonce()
		y = generate_nonce()
		g_y = gmpy2.powmod(self.g, y, self.p)
		message = pack_number(Nt) + pack_number(g_y)
		encrypted_message = RSA_encrypt(message, ClientKey)
		total_message = pack_number(Nc) + pack_bytes(encrypted_message)
		signature = RSA_sign(total_message, self.PrivateKey)
		message_to_send = pack_number(signature) + pack_bytes(total_message)
		s.send_message(message_to_send)											#  [Nc, {Nt, g_y}^kC]^kT
		
		
		# third stage
		# process and verify message 
		recv_message  = s.read_message()
		(signature, recv_message) = unpack_number(recv_message)
		if not RSA_verify(recv_message, signature, ClientKey):
			raise CryptoException('Signature verification failed')
		(Nt_C, recv_message) = unpack_number(recv_message)
		if Nt_C != Nt:
			raise CryptoException('Nonce verification failed')
		
		
		# NS's been completed, derive the session key
		K = gmpy2.powmod(g_x, y, self.p)
		return (hash_key(K), client_name)

	def log_in(self, s, server_name):
		ServerKey = self.read_public_key(server_name)
		
		# first stage
		# compose message
		Nc = generate_nonce()
		x = generate_nonce()
		g_x = gmpy2.powmod(self.g, x, self.p)
		message = pack_number(Nc) + pack_number(g_x)
		encrypted_message = RSA_encrypt(message, ServerKey)						#  {Nc, g_x}^kT
		total_message = pack_bytes(encrypted_message) + pack_bytes(self.name)
		signature = RSA_sign(total_message, self.PrivateKey)
		message_to_send = pack_number(signature) + pack_bytes(total_message)	#  [{Nc, g_x}^kT, C]^kC
		s.send_message(message_to_send)
		
		
		# second stage
		# process message
		recv_message  = s.read_message()										#  [Nc, {Nt, g_y}^kC]^kT
		(signature, recv_message) = unpack_number(recv_message)
		(total_message, recv_message) = unpack_bytes(recv_message)
		if not RSA_verify(total_message, signature, ServerKey):
			raise CryptoException('Signature verification failed')
		(Nc_T, total_message) = unpack_number(total_message)
		if Nc_T != Nc:
			raise CryptoException('Nonce verification failed')
		(encrypted_message, total_message) = unpack_bytes(total_message)
		message = RSA_decrypt(encrypted_message, self.PrivateKey)
		(Nt, message) = unpack_number(message)
		(g_y, message) = unpack_number(message)
		
		# compose the final message
		message = pack_number(Nt)
		signature = RSA_sign(message, self.PrivateKey)
		message_to_send = pack_number(signature) + message
		s.send_message(message_to_send)											#  [Nt]^kC
		
		
		
		# NS's been completed, derive the session key
		K = gmpy2.powmod(g_y, x, self.p)
		return hash_key(K)


	def encrypt(self, plaintext, key):
		plaintext = pad_plaintext(plaintext)
		IV = Random.new().read(AES.block_size)
		aes = AES.new(key[:CryptoUtils.KeyLengthAES], AES.MODE_CBC, IV)
		ciphertext = aes.encrypt(plaintext)
		tag = generate_tag(IV + ciphertext, key[CryptoUtils.KeyLengthAES:]) 
		return IV + ciphertext + tag

	def decrypt(self, ciphertext, key):
		if (len(ciphertext) <= AES.block_size + SHA384.digest_size) or ( (len(ciphertext) - SHA384.digest_size) % AES.block_size != 0):
			raise CryptoException('The length of the ciphertext isn\'t a multiple of the block size')
		tag_start = len(ciphertext) - SHA384.digest_size
		tag = ciphertext[tag_start:]
		IV = ciphertext[:AES.block_size]
		data = ciphertext[AES.block_size:tag_start]
		if not verify_tag(ciphertext, key[CryptoUtils.KeyLengthAES:]):
			raise CryptoException('MAC verification failed')
		aes = AES.new(key[:CryptoUtils.KeyLengthAES], AES.MODE_CBC, IV)
		plaintext = aes.decrypt(data)
		return unpad_plaintext(plaintext)
	
	
	#
	# PKI :-)
	#
	def read_public_key(self, clientName):
		path = 'None'
		try:
			path = 'http://%s/%s.public' % (self.public_keys_path, clientName)
			response = urllib2.urlopen(path)
			key_str = response.read()
			return RSA.importKey(key_str)
		except Exception as ex:
			raise CryptoException('Failed to read %s\'s public key via \'%s\' due to Error: %s' % (clientName, path, ex))
	def read_private_key(self, private_key_file):
		try:
			with open(private_key_file,'r') as f:
				return RSA.importKey(f.read())
		except (OSError, IOError) as ex:
			raise CryptoException(ex)
def generate_RSA_keys():
	key = RSA.generate(CryptoUtils.RSA_KEY_LENGTH)
	private_key = key.exportKey('PEM')
	public_key = key.publickey().exportKey('PEM')
	return (public_key, private_key)




#
# symmetric cryptography helper functions
#
def generate_tag(ciphertext, key):
	return HMAC.new(key, msg=ciphertext, digestmod=SHA384).digest()
def verify_tag(ciphertext, key):
	tag_start = len(ciphertext) - SHA384.digest_size
	data = ciphertext[:tag_start]
	tag = ciphertext[tag_start:]
	correct_tag = generate_tag(data, key)
	return correct_tag == tag
def pad_plaintext(plaintext):
	padding_length = AES.block_size - (len(plaintext) % AES.block_size)
	if padding_length == 0:
		padding_length = AES.block_size
	plaintext += '\x80'
	plaintext += '\x00' * (padding_length-1)
	return plaintext
def unpad_plaintext(plaintext):
	if not plaintext: 
		return plaintext
	plaintext = plaintext.rstrip('\x00')
	if plaintext[-1] == '\x80':
		return plaintext[:-1]
	else:
		return plaintext #???


#
# NS helper functions
#
def generate_nonce():
	return random.randint(1, 1000000)

def int_to_bytes(n):
	s = hex(mpz(n))[2:]
	if len(s) & 1:
		s = '0' + s
	return binascii.unhexlify(s)
def bytes_to_int(b):
	s = binascii.hexlify(b)
	return mpz(s, 16)

def pack_number(n):
	n_bytes = int_to_bytes(n)
	return pack_bytes(n_bytes)
def pack_bytes(n_bytes):
	return struct.pack('>I', len(n_bytes)) + n_bytes
def unpack_number(s):
	n_length = struct.unpack('>I', s[0:4])[0]
	n_bytes = s[4:n_length+4]
	return (bytes_to_int(n_bytes), s[n_length+4:])
def unpack_bytes(s):
	n_length = struct.unpack('>I', s[0:4])[0]
	n_bytes = s[4:n_length+4]
	return (n_bytes, s[n_length+4:])


def RSA_encrypt(message, Key):
	messages = split_message(message, CryptoUtils.RSA_CHUNK_SIZE)
	acc = ''
	for m in messages:
		acc += pack_bytes(Key.encrypt('\x01' + m, 0)[0])
	return acc
def RSA_decrypt(encrypted_message, Key):
	message = ''
	while encrypted_message:
		(enc_m, encrypted_message) = unpack_bytes(encrypted_message)
		message += Key.decrypt(enc_m)[1:]
	return message

def split_message(message, n):
	length = len(message) / n
	if len(message) % n:
		length += 1
	return [message[i*n:(i+1)*n]  for i in xrange(length)]

def RSA_sign(message, Key):
	h = SHA384.new()
	h.update(message)
	return Key.sign(h.hexdigest(), 0)[0]
def RSA_verify(message, signature, Key):
	h = SHA384.new()
	h.update(message)
	message = h.hexdigest()
	return bytes_to_int(message) == Key.encrypt(int(signature), 0)[0]

def hash_key(K):
	h = SHA384.new()
	h.update(int_to_bytes(K))
	return h.hexdigest()
