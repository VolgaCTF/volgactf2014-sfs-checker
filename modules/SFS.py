#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import random
from hashlib import sha224


class SfsException(Exception):
	pass
class SFS:
	def __init__(self, numOfCVs, maxFiles, fileLength, path, keyGenerationPassword):
		self.numOfCVs = numOfCVs
		self.maxFiles = maxFiles
		self.fileLength = fileLength

		self.path = path
		if not os.path.exists(path):
			try:
				os.mkdir(path, 0755)
			except Exception as ex:
				raise SfsException(ex)
		if not os.path.isdir(path):
			raise SfsException('Path <%s> doesn\'t point to a directory' % path)

		self.K = generate_key(self.maxFiles, self.numOfCVs, keyGenerationPassword)
		self.create_shadow_files()


	#
	# file embedding and extraction
	#
	def create_shadow_files(self):
		try:
			if not os.path.isfile(os.path.join(self.path, 'shadow_files')):
				C = []
				for i in xrange(self.numOfCVs):
					C.append(map(ord, list(os.urandom(self.fileLength))))
				self.write_shadow_files(C)
		except (OSError, IOError) as ex:
			raise SfsException('Failed to create shadow files due to ERROR: ' + str(ex))

	def read_shadow_files(self):
		try:
			with open(os.path.join(self.path, 'shadow_files'), 'rb') as f:
				str = f.read()
			return [map(ord, str[i:i+self.fileLength])  for i in range(0, len(str), self.fileLength)]
		except (OSError, IOError) as ex:
				raise SfsException(ex)

	def write_shadow_files(self, C):
		try:
			with open(os.path.join(self.path, 'shadow_files'), 'wb') as f:
				f.write(''.join([''.join(map(chr, Ci))  for Ci in C]))
		except (OSError, IOError) as ex:
			raise SfsException(ex)



	def extract_files(self):
		Files_str = ['' for i in xrange(self.maxFiles)]
		for i in xrange(self.maxFiles):
			Files_str[i] = self.extract_file(i)
		return Files_str

	def extract_file(self, j):
		C = self.read_shadow_files()
		return file_to_string(self.extract_file_raw(j, C))

	def extract_file_raw(self, j, C):
		if j >= self.maxFiles:
			raise SfsException('The asked file number %d exceeds the maximum number of files' % j)
		F = [0 for i in xrange(self.fileLength)]
		for i in xrange(len(self.K[j])):
			if self.K[j][i] != 0:
				F = XOR(F, C[i])
		return F



	def embed_files(self, Files_str):
		for i in xrange(len(Files_str)):
			self.embed_file(i, Files_str[i])

	def embed_file(self, j, F_str):
		if j >= self.maxFiles:
			raise SfsException('The asked file number %d exceeds the maximum number of files' % j)
		C = self.read_shadow_files()
		F = string_to_file(F_str, self.fileLength)
		D = XOR(self.extract_file_raw(j, C), F)
		for i in xrange(len(self.K[j])):
			if self.K[j][i] != 0:
				C[i] = XOR(C[i], D)
		self.write_shadow_files(C)



#
# key generation
#
def generate_key(m, k, keyGenerationPassword):
	random.seed(int(sha224(keyGenerationPassword).hexdigest(), 16))
	K = [[] for t in xrange(m)]
	j = 0
	while j < m:
		# generate a new key
		K[j] = [r%2 for r in [random.randint(0, 10000)  for i in xrange(k)]]
		uj = K[j]

		# orthogonalise
		for t in xrange(j):
			proj(uj, K[t], K[j], k)

		# check to see if the norm of uj is 0
		if norm(uj) != 0:
			K[j] = uj
			j += 1

	return K

def proj(uj, u, v, k):
	inn_pr = inner_product(u, v)
	for i in xrange(k):
		uj[i] = (uj[i] - inn_pr*u[i]) % 2
def inner_product(u, v):
	inn_pr = 0;
	for i in xrange(len(u)):
		inn_pr += u[i]*v[i];
	return inn_pr % 2;
def norm(u):
	return inner_product(u, u)


#
# helper functions
#
def XOR(A, B):
	return [A[i]^B[i] for i in xrange(min(len(A), len(B)))]

def string_to_file(str, fileLength):
	if len(str) >= fileLength:
		str = str[:fileLength]
	else:
		str += '\x00'*(fileLength-len(str))
	return map(ord, list(str))
def file_to_string(F):
	str = ''.join(map(chr, F))
	return str.strip('\x00')

def generate_SFS_password():
	s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*/!@#$%^&*()_=+\\/'
	p = ''
	for i in xrange(25):
		p += s[random.randint(0, 1000000) % len(s)]
	return p



#
# sanity check
#
def check_key(StegFS):
	outputMatrix(StegFS.K)
	print ''
	N = [[0 for i in xrange(StegFS.maxFiles)]  for j in xrange(StegFS.maxFiles)]
	for i in xrange(StegFS.maxFiles):
		for j in xrange(StegFS.maxFiles):
			N[i][j] = inner_product(StegFS.K[i], StegFS.K[j])
	outputMatrix(N)
def output_matrix(M):
	for i in xrange(len(M)):
		print ' '.join(map(str, M[i]))